# Fedora xe-guest-utilities 

## Install

`sudo dnf install xe-guest-utilities-latest`

Put these files in `/usr/lib/systemd/system/`

- proc-xen.mount
- xe-daemon.service
- xe-linux-distribution.service `This is installed with xe-guest-utilities-latest`

To enable these services:
`sudo systemctl enable xe-linux-distribution && sudo systemctl enable xe-daemon`

Then reboot!

Forked from:
https://aur.archlinux.org/cgit/aur.git/tree/?h=xe-guest-utilities
https://aur.archlinux.org/xe-guest-utilities.git/

